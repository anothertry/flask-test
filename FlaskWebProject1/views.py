"""
Routes and views for the flask application.
"""

import sqlite3
from flask import Flask, request, render_template, session
from datetime import date

from FlaskWebProject1 import app
# app = Flask(__name__)

app.secret_key = 'Ninja'


@app.route('/')
@app.route('/index')
def about():
    """Renders the home page."""
    return render_template('index.html')


@app.route('/galery')
def contact():
    """Renders the contact page."""
    return render_template('galery.html')


@app.route('/comments', methods=['GET'])
def comments_get():
    with sqlite3.connect('FlaskWebProject1\example.db') as connection:
        cursor = connection.cursor()
        last_visit = request.cookies['lastVisit'] if 'lastVisit' in request.cookies else 'Hello newcomer'
        try:
            today_visit, total_visit, hits = visits_info(cursor)
            temp = last_visit.split(' ')
            last_visit = ' '.join(temp[:5])
            return render_template(
                "comments.html",
                # cursor=[s for s in cursor.execute('SELECT * FROM comments')],
                cursor=[unicode(s[0]) for s in cursor.execute('SELECT * FROM comments')],
                last_visit=last_visit,
                today_visit=split_number(today_visit),
                total_visit=split_number(total_visit),
                hits=split_number(hits)
            )
        finally:
            cursor.close()


def split_number(number):
    if number == 0:
        return [0]
    digits = []
    while number > 0:
        digits.append(number % 10)
        number //= 10
    return reversed(digits)



def visits_info(cursor):
    last_visit = cursor.execute("SELECT lastDate FROM visits").fetchone()[0]
    year, month, day = map(int, last_visit.split('-'))
    last_visit = date(year, month, day)
    if last_visit != date.today():
        cursor.execute("UPDATE visits SET today = 0")
        cursor.execute("UPDATE visits SET lastDate = '{0}'".format(date.today()))
    if 'auth' not in session:
        cursor.execute("UPDATE visits SET today = today + 1, total = total + 1")
        session['auth'] = None
    cursor.execute("UPDATE visits SET hits = hits + 1")
    return cursor.execute("SELECT today, total, hits FROM visits").fetchone()


@app.route('/comments', methods=['POST'])
def comments_post():
    with sqlite3.connect('FlaskWebProject1\example.db') as connection:
        cursor = connection.cursor()
        if request.form['comment']:
            cursor.execute(u"INSERT INTO comments VALUES ('{0}')".format(request.form['comment']))
        cursor.close()
    return comments_get()


# if __name__ == '__main__':
#   app.run(host='localhost', port=80, debug=True)
