import sqlite3


with sqlite3.connect("example.db") as connection:
    cursor = connection.cursor()
    cursor.execute("DROP TABLE comments")
    cursor.execute("CREATE TABLE comments (comment text)")
    cursor.execute("DROP TABLE visits")
    cursor.execute("CREATE TABLE visits (total int, today int, hits int, lastDate date)")
    cursor.execute("INSERT INTO visits VALUES (0, 0, 0, '2017-12-10')")


# "CREATE TABLE visits (total int, today int, lastDate date)"
# "DROP TABLE visits"
# "CREATE TABLE comments (comment text)"
# "INSERT INTO visits VALUES (0, 0, '2017-12-6')"
# "UPDATE visits SET total = total + 1"
# "UPDATE visits SET today = today + 1"
