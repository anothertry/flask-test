var anyImgIsShowing = false;
var imgCount = 4;
var showImgNum;
var isLoaded = [];
var isLoading = [];
for (var i = 0; i < imgCount; i++){
    isLoaded.push(false);
    isLoading.push(false);
}


function clear(){
    document.getElementById("loading").style.display = "none";
    for (var i = 0; i < imgCount; i++){
        var currentShowImg = document.getElementById("showimg" + i.toString());
        if (currentShowImg)
            currentShowImg.style.display = "none"
    }
    var modal = document.getElementById("modal");
    modal.style.display = "none";
}

function mypopstate(){
    if (document.location.hash.length != 0){
        var imgNum = document.location.hash.substring(1);
        document.getElementById("img" + imgNum).click();
    }
    else {
        clear();
    }
}

window.addEventListener("popstate", mypopstate);

function showimg(event){
    anyImgIsShowing = true;
    clear();
    
    var target = event.srcElement || event.target;
    showImgNum = parseInt(target.id[3]);
    
    var modal = document.getElementById("modal");
    modal.style.display = "block";
    window.location.hash = "#" + showImgNum.toString();
    setShowImgCaption(target);
    
    if (isLoaded[showImgNum]){
        document.getElementById("showimg" + showImgNum.toString()).style.display = "block";
        return false;
    }
    
    document.getElementById("loading").style.display = "block";
    
    if (isLoading[showImgNum]){
        return false;
    }
    
    createShowImgElement(target, showImgNum);
}

function setShowImgCaption(target){
    var caption = document.getElementById("caption");
    caption.innerHTML = target.alt;
}

function createShowImgElement(target, num){
    var modal = document.getElementById("modal");
    var modalImg = document.createElement("img");
    modalImg.src = target.src.replace(".thumb", "");
    modalImg.alt = target.alt;
    modalImg.className = "modal-content";
    modalImg.style.display = "none";
    modalImg.id = "showimg" + num.toString();
    modalImg.onload = onLoadImg;
    modal.insertBefore(modalImg, modal.firstChild);
    isLoading[num] = true;
}

function onLoadImg(event){
    var target = event.target || event.srcElement;
    var loadedImgNum = parseInt(target.id[7]);
    isLoaded[loadedImgNum] = true;
    isLoading[loadedImgNum] = false;
    if (loadedImgNum == showImgNum){
        document.getElementById("loading").style.display = "none";
        target.style.display = "block";
        setShowImgCaption(target);
    }
    var nextShowImg = (showImgNum + 1) % imgCount;
    if (isLoaded[nextShowImg] || isLoading[nextShowImg]){
        return false;
    }
    var target = document.getElementById("img" + nextShowImg.toString());
    createShowImgElement(target, nextShowImg);
}

function closeimg(e){
    if (e.keyCode == 112){
        alert(
            "Эскейп закрытие фото\n" +
            "Левая, правая стрелочки для листания изображений\n" +
            "Пробел сделать заставкой"
        );
        e.preventDefault ? e.preventDefault() : (e.returnValue = false);
    }
    if (anyImgIsShowing){
        if (e.keyCode == 27 || e.keyCode == 39 || e.keyCode == 37){            
            var imgShow = document.getElementById("showimg" + showImgNum.toString());
            imgShow.style.display = "none";
            if (e.keyCode == 27){
                anyImgIsShowing = false;
                var modal = document.getElementById("modal");
                modal.style.display = "none";
                window.location = window.location.href.substring(0, window.location.href.indexOf('#'));
            }
            if (e.keyCode == 39){
                document.getElementById("img" + ((showImgNum + 1) % imgCount).toString()).click();
            }
            if (e.keyCode == 37){
                if (showImgNum == 0){
                    document.getElementById("img" + (imgCount - 1).toString()).click();
                } else {
                    document.getElementById("img" + (showImgNum - 1).toString()).click();
                }
            }
        }
        if (e.keyCode == 32){
            var body = document.getElementById("body");
            body.style.backgroundImage = "url({0})".replace("{0}", document.getElementById("showimg" + showImgNum.toString()).src);
            document.cookie = "bgImage={0}".replace("{0}", document.getElementById("showimg" + showImgNum.toString()).src);
        }
    }
}

function onLoad(){
    for (var i = 0; i < imgCount; i++){
        document.getElementById("img" + i.toString()).onclick = showimg;
    }
    if (document.location.hash.length != 0){
        var imgNum = document.location.hash.substring(1);
        document.getElementById("img" + imgNum).click();
    }
}