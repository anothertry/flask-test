function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

var bgSrc = getCookie("bgImage");
if (bgSrc){
    var body = document.getElementById("body");
    body.style.backgroundImage = "url({0})".replace("{0}", bgSrc);
}